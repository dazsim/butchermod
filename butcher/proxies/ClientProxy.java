package butcher.proxies;

import net.minecraft.client.model.ModelPig;
import net.minecraft.client.renderer.entity.RenderPig;
import butcher.entity.passive.EntityMyPig;
import butcher.tileentities.TESRCampfire;
import butcher.tileentities.TileEntityCampfire;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {
	
	@Override
	public void initSounds() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initRenderers() {
		// TODO Auto-generated method stub
		 
		
	}
	@Override
    public void registerRenderers() 
	{
    	
    	RenderingRegistry.registerEntityRenderingHandler(EntityMyPig.class, new RenderPig(new ModelPig(), new ModelPig(0.5F), 0.7F));
    	ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCampfire.class, new TESRCampfire());
    }

}

