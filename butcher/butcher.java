package butcher;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityEggInfo;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.world.biome.BiomeGenBase;
import butcher.blocks.blockCampfire;
import butcher.entity.passive.EntityMyPig;
import butcher.proxies.CommonProxy;
import butcher.tileentities.TileEntityCampfire;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
//import net.minecraft.block.StepSoundStone;
import cpw.mods.fml.common.registry.LanguageRegistry;


@Mod(modid="butcher", name="Butcher mod", version = "0.0.1")
@NetworkMod(channels = ("butchermod"),clientSideRequired = true, serverSideRequired = true, packetHandler = PacketHandler.class)
public class butcher {
	@Instance static butcher instance;
	
	private static Block bCampfire;
	// static blockCampfire mycampfire;
	
	//mycampfire.setHardness(1.5F);
	//.setResistance(10F)
	//.setStepSound(null);
	@EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            removeAnimals();
            
    }
	
	
	@SidedProxy(clientSide = "butcher.proxies.ClientProxy", serverSide = "butcher.proxies.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Stub Method
		proxy.initSounds();
		proxy.initRenderers();
	}
	@EventHandler
	public void load(FMLInitializationEvent event) {
		proxy.registerRenderers();
		addMyAnimals();
		registerEntityEgg(EntityMyPig.class, 0x999999, 0xFF0000, 5001);
		//CraftingManager.addRecipe(new ItemStack(), null);
		bCampfire = new blockCampfire(3001).setHardness(0.5F).setStepSound(Block.soundGravelFootstep)
				.setUnlocalizedName("blockCampfire").setCreativeTab(CreativeTabs.tabBlock);
		LanguageRegistry.addName(bCampfire, "Campfire");
		//final CraftingManager cm1;
		CraftingManager cm1;
		//cm1.addRecipe(new ItemStack(Block.furnaceIdle), new Object[] {"###", "#X#", "###", '#', Block.cobblestone, 'X', Block.wood});
		GameRegistry.registerBlock(bCampfire,"blockCampfire");
		
		
	}
	
	public static void registerTileEntities() {
			GameRegistry.registerTileEntity(TileEntityCampfire.class,"campfireTileEntity");
		}
	
	private void addMyAnimals()
	{
       // EntityRegistry.addSpawn(EntityMyPig.class,1000,1,4, EnumCreatureType.creature, BiomeGenBase.biomeList[1]);
       // EntityRegistry.removeSpawn(EntityPig.class, EnumCreatureType.creature, BiomeGenBase.biomeList[1]);
       // EntityRegistry.removeSpawn(EntityCow.class, EnumCreatureType.creature, BiomeGenBase.biomeList[1]);
       // EntityRegistry.removeSpawn(EntitySheep.class, EnumCreatureType.creature, BiomeGenBase.biomeList[1]);
       // EntityRegistry.removeSpawn(EntitySlime.class, EnumCreatureType.monster, BiomeGenBase.biomeList[1]);
        
        
	}
	
	
	
	private void removeAnimals() 
	{
		 
        for (int i = 0; i < BiomeGenBase.biomeList.length; i++)
                if (BiomeGenBase.biomeList[i] != null) 
                {
                        BiomeGenBase biome = BiomeGenBase.biomeList[i];
                        //this removes vanilla pig from worldgen
                        EntityRegistry.removeSpawn(EntityPig.class, EnumCreatureType.creature, new BiomeGenBase[] { biome });
                        //this adds our custom pig to the worldgen
                        
                        
                        
                       // EntityRegistry.removeSpawn(EntitySheep.class,
                        //        EnumCreatureType.creature, new BiomeGenBase[] { biome });
                      //  EntityRegistry.removeSpawn(EntityChicken.class,
                         //       EnumCreatureType.creature, new BiomeGenBase[] { biome });
                      //  EntityRegistry.removeSpawn(EntityCow.class,
                        //        EnumCreatureType.creature, new BiomeGenBase[] { biome });
                }
		}
	
	public butcher ()
	{
		//constructor for butcher mod
		
	}

	
	public static void registerEntityEgg(Class<? extends Entity>entity, int primaryColor, int secondaryColor, int eid) 
	{
		int id = eid;
		EntityList.IDtoClassMapping.put(id, entity);
		EntityList.entityEggs.put(id, new EntityEggInfo(id, primaryColor, secondaryColor));
		
	}
	
}
